name        "cookbook-base"
description 'The base (core) recipes applied to all nodes'
maintainer  "MyOrg"
license     "Apache 2.0"

depends 'cookbook-motd'